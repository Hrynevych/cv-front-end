import MainPage from "./Component/main";
import {BrowserRouter} from "react-router-dom";
import "./styles/themes/themes.scss";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <MainPage />
            </BrowserRouter>
        </div>
    );
}

export default App;
