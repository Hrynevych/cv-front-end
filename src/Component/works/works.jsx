import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Footer from "../footer";
import Work from "./work";
import {faChevronDown} from "@fortawesome/free-solid-svg-icons";
import ph1 from "../../assets/img/Project1/5.png";
import ph2 from "../../assets/img/Project2/1.png";
import ph3 from "../../assets/img/Project3/1.png";
const Works = () => {
    const projects = [
        {
            id: 3,
            name: "Platform for buying goods",
            type: "Development",
            data: "January, 2023",
            img: ph3,
            link: "https://ecom-git-dev-hrynevych.vercel.app/",
            stack:'Next.js SCSS TypeScript Firebase Eslint'
        },
        {
            id: 1,
            name: "Quiz Platform",
            type: "Development",
            data: "March, 2022",
            img: ph1,
            link: "https://melodic-haupia-ac992c.netlify.app",
            stack: "React SCSS JSON TypeScript DragAndDrop",
        },
        {
            id: 2,
            name: "Portfolio Application",
            type: "Development",
            data: "November, 2021",
            img: ph2,
            link: "https://ecstatic-kare-85373d.netlify.app",
            stack: "React HTML CSS",
        },
    ];
    return (
        <>
            <div className="works">
                <div className="works__wrapper">
                    <div className="works__title">
                        <h2>
                            The work i do,
                            <br />
                            and bussines i help.
                        </h2>
                        <div className="works__lastProj">
                            <p>Latest projects</p>
                            <FontAwesomeIcon size="xs" icon={faChevronDown} />
                        </div>
                    </div>
                    {projects.map((el) => (
                        <Work
                            key={el.id}
                            name={el.name}
                            type={el.type}
                            data={el.data}
                            img={el.img}
                            link={el.link}
                            stack={el.stack}
                        />
                    ))}
                </div>
            </div>
            <Footer />
        </>
    );
};

export default Works;
<div></div>;
