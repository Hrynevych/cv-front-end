import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

const Work = ({name, type, data, img, link,stack}) => {
    return (
        <div className="work">
            <div className="work__img">
                <img src={img} alt={name} className="work__image" />
            </div>
            <div className="work__title">
                <div className="work__link" onClick={() => window.open(link)}>
                    <h2>{name}</h2>
                    <FontAwesomeIcon
                        color="#b6b9c9"
                        size="xl"
                        icon={faArrowRight}
                    />
                </div>
                <div className="work__stack">
                    <p>{stack}</p>
                </div>
                <div className="work__titleTypeData">
                    <p>{type}</p>

                    <p>{data}</p>
                </div>
            </div>
        </div>
    );
};

export default Work;
