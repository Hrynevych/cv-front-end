import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Form from "../feedback/form";
import Footer from "../footer";
import {
    faEnvelope,
    faMobileScreenButton,
    faLocationDot,
} from "@fortawesome/free-solid-svg-icons";

const Contact = ({email}) => {
    return (
        <div className="contact">
            <div className="contact__intro">
                <div className="contact__text">
                    <h1>We've been </h1>
                    <h2>waiting for you.</h2>
                    <p  className="--fs2rem firstP">
                        Fill in the form or
                        <span>
                            <a href="mailto:vovkahrynevych@gmail.com">
                                Send us an email
                            </a>
                        </span>
                    </p>
                    <div className="contact__phone-wrapper --pt3rem">
                        <div className="contact__phone --after">
                            <FontAwesomeIcon
                                size="2x"
                                color="#9A83F4"
                                icon={faMobileScreenButton}
                            />
                        </div>
                        <p>+ 48 730 847 772</p>
                    </div>
                    <div className="contact__email-wrapper --pt3rem">
                        <div className="contact__email --after">
                            <FontAwesomeIcon
                                size="2x"
                                color="#9A83F4"
                                icon={faEnvelope}
                            />
                        </div>
                        <p>vovkahrynevych@gmail.com</p>
                    </div>
                    <div className="contact__location-wrapper --pt3rem">
                        <div className="contact__location --after">
                            <FontAwesomeIcon
                                size="2x"
                                color="#9A83F4"
                                icon={faLocationDot}
                            />
                        </div>
                        <p>Lublin / Lviv</p>
                    </div>
                </div>
                <Form email={email}/>
            </div>

            <div className="contact__gap"></div>
            <Footer />
        </div>
    );
};

export default Contact;
