import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faInstagram} from "@fortawesome/free-brands-svg-icons";
import {faBitbucket} from "@fortawesome/free-brands-svg-icons";
import {faFacebook} from "@fortawesome/free-brands-svg-icons";
import {faLinkedin} from "@fortawesome/free-brands-svg-icons";
import i from "../../assets/img/i.png";

import {NavLink} from "react-router-dom";
import {useState} from "react";
import Modal from "../feedback/modal";

const NavBar = () => {
    const [isOpenMenu, setIsOpenMenu] = useState(false);
    const icon = [
        {
            id: 1,
            size: "lg",
            color: "#656B8E",
            icon: faInstagram,
            link: "https://www.instagram.com/hrynevych.vol/",
        },
        {
            id: 2,
            size: "lg",
            color: "#656B8E",
            icon: faBitbucket,
            link: "https://bitbucket.org/Hrynevych/",
        },
        {
            id: 3,
            size: "lg",
            color: "#656B8E",
            icon: faFacebook,
            link: "https://www.facebook.com/vivahrynevych/",
        },
        {
            id: 4,
            size: "lg",
            color: "#656B8E",
            icon: faLinkedin,
            link: "https://www.linkedin.com/in/volodymyr-hrynevych-178443210/",
        },
    ];
    return (
        <div className="navBar">
            <div className="navBar__logo">
                <img src={i} alt="" />
            </div>
            <div className="navBar__menu">
                <NavLink activeclassname='active' to="/">
                    <p>About</p>
                </NavLink>
                <NavLink to="/works">
                    <p>Works</p>
                </NavLink>
                <NavLink to="/contact">
                    <p>Contact</p>
                </NavLink>
            </div>
            <nav className="navBar__link">
                {icon.map((el) => {
                    return (
                        <FontAwesomeIcon
                            key={el.id}
                            size={el.size}
                            color={el.color}
                            icon={el.icon}
                            onClick={() => window.open(el.link)}
                        />
                    );
                })}
            </nav>
            <nav className="navBar__mobile">
                <div
                    className="navBar__burgerWrapper"
                    onClick={() => setIsOpenMenu(!isOpenMenu)}
                >
                    <div
                        className={`navBar__burger ${
                            isOpenMenu ? "active" : "disable"
                        } `}
                    ></div>
                    {isOpenMenu && (
                        <Modal>
                            <div className="navBar__burgerItem">
                                <div className="navBar__burgerMenu">
                                    <NavLink to="/">
                                        <p>About</p>
                                    </NavLink>
                                    <NavLink to="/works">
                                        <p>Works</p>
                                    </NavLink>
                                    <NavLink to="/contact">
                                        <p>Contact</p>
                                    </NavLink>
                                </div>
                                <div className="navBar__burgerLink">
                                    {icon.map((el) => {
                                        return (
                                            <FontAwesomeIcon
                                                key={el.id}
                                                size={el.size}
                                                color={el.color}
                                                icon={el.icon}
                                                onClick={() =>
                                                    window.open(el.link)
                                                }
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                        </Modal>
                    )}
                </div>
            </nav>
        </div>
    );
};

export default NavBar;
