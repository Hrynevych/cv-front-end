import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faInstagram} from "@fortawesome/free-brands-svg-icons";
import {faFacebook} from "@fortawesome/free-brands-svg-icons";
import {faBitbucket} from "@fortawesome/free-brands-svg-icons";
import {faLinkedin} from "@fortawesome/free-brands-svg-icons";
import i from "../../assets/img/i.png";

import {Link} from "react-router-dom";

const Footer = () => {
    const icon = [
        {
            id: 1,
            size: "lg",
            color: "#656B8E",
            icon: faInstagram,
            link: "https://www.instagram.com/hrynevych.vol/",
        },
        {
            id: 2,
            size: "lg",
            color: "#656B8E",
            icon: faBitbucket,
            link: "https://bitbucket.org/Hrynevych/",
        },
        {
            id: 3,
            size: "lg",
            color: "#656B8E",
            icon: faFacebook,
            link: "https://www.facebook.com/vivahrynevych/",
        },
        {
            id: 4,
            size: "lg",
            color: "#656B8E",
            icon: faLinkedin,
            link: "https://www.linkedin.com/in/volodymyr-hrynevych-178443210/",
        },
    ];
    return (
        <footer className="footer">
            <div className="footer__wrapper">
                <div className="footer__wrapper-info">
                    <div className="footer__info">
                        <div className="footer__logo">
                            <img src={i} alt="" />
                        </div>
                        <div className="footer__name">
                            <h2> Hrynevych Volodymyr </h2>
                            <p>Front-End Developer </p>
                        </div>
                        <div className="footer__email">
                            <p>Email me at</p>
                            <p>vovkahrynevych@gmail.com</p>
                        </div>
                    </div>
                    <div className="footer__text">
                        <h2>Lets Talk!</h2>
                        <p>
                            I am always open to discussing your project, <br />{" "}
                            improving your online presence, or helping with
                            yours <br /> web applications.
                        </p>
                        <div className="footer__links">
                            {icon.map((el) => {
                                return (
                                    <FontAwesomeIcon
                                        key={el.id}
                                        size={el.size}
                                        color={el.color}
                                        icon={el.icon}
                                        onClick={() => window.open(el.link)}
                                    />
                                );
                            })}
                        </div>
                    </div>
                </div>
                <div className="footer__routing">
                    <Link to="/">
                        <p>About</p>
                    </Link>
                    <Link to="/works">
                        <p>Works</p>
                    </Link>
                    <Link to="/contact">
                        <p>Contact</p>
                    </Link>
                    <div className="footer__right">
                        <p>© 2022 Made by Hrynevych Volodymyr </p>
                        <p>All rights reserved</p>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
