const Counters = () => {
    return (
        <div className="counters">
            <div>
                <h2>5</h2>
                <p>projects completed</p>
            </div>
            <div className="counters__circle">
                <div></div>
            </div>
            <div>
                <h2>1.5+</h2>
                <p>years experience</p>
            </div>
            <div className="counters__circle">
                <div></div>
            </div>
            <div>
                <h2>1M+</h2>
                <p>written code</p>
            </div>
            <div className="counters__circle">
                <div></div>
            </div>
            <div>
                <h2>10+</h2>
                <p>libraries</p>
            </div>
        </div>
    );
};

export default Counters;
