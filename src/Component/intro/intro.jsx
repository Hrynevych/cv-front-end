import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPenRuler} from "@fortawesome/free-solid-svg-icons";
import {useNavigate} from "react-router-dom";
import {useState} from "react";

const Intro = ({email}) => {
    const [inputValue, setinputValue] = useState("");
    const navigate = useNavigate();
    const sendEmail = (inputValue) => {
        email(inputValue);
        setinputValue("");
        navigate("/contact");
    };
    return (
        <div className="intro">
            <p className="intro__title">
                Building front-end
                <br />
                applications, commercial
            </p>
            <div className="intro__exp">
                <div className="intro__icon">
                    <FontAwesomeIcon color="white" icon={faPenRuler} />
                </div>
                <p>experience.</p>
            </div>

            <p className="intro__text">
                I rely on the most outstanding open-source libraries <b />
                <span>React & Redux</span>
                <br /> these are my tools with which I able to solve any
                challenges <br /> that arise in my work life.
            </p>
            <div className="intro__email">
                <input
                    onChange={(e) => setinputValue(e.target.value)}
                    type="email"
                    placeholder="Email"
                />
                <button onClick={() => sendEmail(inputValue)}>
                    Connect With Me
                </button>
            </div>
        </div>
    );
};
export default Intro;
