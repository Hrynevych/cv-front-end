import Icon from "../Icon";

const Service = ({number, name, text}) => {
    return (
        <div className="service">
            <div className="service__img">
                <Icon name={name}/>
            </div>
            <div className="service__content">
                <span className="service__number">{number}</span>
                <h4 className="service__name">{name}</h4>
                <p className="service__text">{text}</p>
            </div>
            <div className="service__arrow">
                <Icon name="Arrow" />
            </div>
        </div>
    );
};

export default Service;
