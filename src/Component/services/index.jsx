import Service from "../services/service";

const Services = () => {
    return (
        <div className="services">
            <h2>
                As a professional, I can provide a wide range of services to
                make sure you have
            </h2>

            <h3>everynthing under control</h3>
            <div className="services__item">
                <Service
                    number="1"
                    name="Ux/Ui"
                    text="Thorough research to prepare  the best solutions"
                />
                <Service
                    number="2"
                    name="Web design"
                    text="Full responsive and equipped  with modern technology"
                />
                <Service
                    number="3"
                    name="Applications"
                    text="Individually planned solutions to provide best answers to your problems and needs"
                />

                <Service
                    number="4"
                    name="Software"
                    text="We examine structures of your company to find the best way to improve its operation"
                />
            </div>
        </div>
    );
};

export default Services;
