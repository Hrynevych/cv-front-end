import Icon from "../Icon/index";
const SendButton = ({onClick,text,icon}) => {
    return (
        <div className="send-button" onClick={onClick}>
            <p>{text}</p>
            <Icon name={icon} />
        </div>
    );
};

export default SendButton;
