import { useState } from "react";
import SendButton from "../button/send-button";
import Form from "./form";
import Modal from "./modal";
const Feedback = () => {
    const [isModalActive,setIsModalActive]= useState(false);
    return (
        <div className="feedback">
            <h2>
                Clients’ <br /> experience
            </h2>
            <div className="feedback__response">
                <p>
                    Creating such a unique and effective solution for our
                    company, which allowed to reduce production costs thanks to
                    new software and in such a short time is a real challenge
                    and success.
                </p>
            </div>
            <SendButton onClick={()=> setIsModalActive(true)} text='Contact me' icon='Send'/>
            <Modal active={isModalActive} setActive={setIsModalActive}>
                <Form isModalActive={setIsModalActive}/>
            </Modal>
        </div>
    );
};

export default Feedback;
