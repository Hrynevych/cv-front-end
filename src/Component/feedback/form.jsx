import emailjs from "@emailjs/browser";
import {useEffect} from "react";
import {useState} from "react";
import {useRef} from "react";

const Form = (props) => {
    console.log(props);
    const form = useRef();
    const [isDisable, setIsDisable] = useState(true);
    const [email, setEmail] = useState("");
    const [error, setError] = useState(null);

    function isValidEmail(email) {
        return /\S+@\S+\.\S+/.test(email);
    }

    const handleChange = (event) => {
        if (!isValidEmail(event.target.value)) {
            setError("Email is novalid");
            setIsDisable(true);
        } else {
            setError(null);
            setIsDisable(false);
        }

        setEmail(event.target.value);
    };
    const sendEmail = (e) => {
        e.preventDefault();

        emailjs
            .sendForm(
                "service_2aiqrxq",
                "template_8imcgas",
                form.current,
                "VVGuU7Hl7rEJItEG-",
            )
            .then(
                (result) => {
                    console.log(result.text);
                    alert("Great");
                    e.target.reset();
                },
                (error) => {
                    alert(error.text);
                },
            );
    };
    useEffect(() => {
        setEmail(props.email);
    }, [props.email]);
    return (
        <div className="form">
            <form ref={form} onSubmit={sendEmail}>
                <div className="form__title">Send us a Message</div>
                <div className="form__name">
                    <input
                        id="name"
                        name="name"
                        className="form__input"
                        type="text"
                        placeholder=" "
                    />
                    <div className="form__cut"></div>
                    <label htmlFor="name" className="form__placeholder">
                        Name
                    </label>
                </div>
                <div className="form__email">
                    <input
                        id="email"
                        name="email"
                        value={email}
                        onChange={handleChange}
                        className="form__input"
                        type="email"
                        placeholder=" "
                    />
                    <div className="form__cut"></div>
                    <label htmlFor="email" className="form__placeholder">
                        Email
                    </label>
                    {error && (
                        <p
                            style={{
                                color: "red",
                                marginLeft: "5rem",
                                marginTop: "5px",
                            }}
                        >
                            {error}
                        </p>
                    )}
                </div>
                <div className="form__subject">
                    <input
                        id="subject"
                        name="subject"
                        className="form__input"
                        type="text"
                        placeholder=" "
                    />
                    <div className="form__cut"></div>
                    <label htmlFor="subject" className="form__placeholder">
                        Subject
                    </label>
                </div>

                <div className="form__textarea">
                    <textarea
                        name="textarea"
                        id="textarea"
                        type="text"
                        className="form__input"
                        placeholder="Tell me about your project..."
                    ></textarea>
                </div>
                <button
                    disabled={isDisable}
                    className="form__submit"
                    type="submit"
                >
                    Send Message
                </button>
            </form>
        </div>
    );
};

export default Form;
