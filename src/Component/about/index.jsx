import dev from "../../assets/img/sub.png";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAddressCard} from "@fortawesome/free-solid-svg-icons";
import Footer from "../footer";
import Intro from "../intro/intro";
import {useState} from "react";
import Technology from "../technology";

const About = ({email}) => {
    const [isOpen, setIsOpen] = useState(false);
    return (
        <>
            <Intro email={email} />
            <div className="about">
                <div className="about__img">
                    <img src={dev} alt="123" />
                </div>
                <div className="about__text">
                    <p>Who is a Volodymyr Hrynevych</p>
                    <div className="about__me">
                        <div>
                            <FontAwesomeIcon
                                color="white"
                                icon={faAddressCard}
                            />
                        </div>

                        <h2>A Bit About Me</h2>
                    </div>
                    <p>
                        I'm an enthusiastic and detail-oriented Frontend
                        Software Engineer <br /> with more than a year of
                        experience.
                    </p>
                    <p>
                        I use my coding skills to comprehensively troubleshoot
                        <br /> and help complete projects on time.
                    </p>

                    <button onClick={() => setIsOpen(!isOpen)}>
                        Learn More
                    </button>
                </div>
            </div>
            <Technology />
            <Footer />
        </>
    );
};

export default About;
