import NavBar from "../nav/nav";
import Contact from "../contact";

import {Routes, Route} from "react-router-dom";
import Works from "../works/works";
import About from "../about";
import {useState} from "react";

const MainPage = () => {
    const [email, setEmail] = useState("");
    const handleEmailChange = (email) => {
        setEmail(email);
    };
    return (
        <div className="main-page">
            <div className="main-page__wrapper">
                <NavBar />
                <Routes>
                    <Route
                        path="/"
                        element={<About email={handleEmailChange} />}
                    />
                    <Route
                        path="/contact"
                        element={<Contact email={email} />}
                    />
                    <Route path="/works" element={<Works />} />
                </Routes>
            </div>
        </div>
    );
};
export default MainPage;
