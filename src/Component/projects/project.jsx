import {Carousel} from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import project1img1 from "../../assets/img/Project1/1.png";
import project1img2 from "../../assets/img/Project1/2.png";
import project1img3 from "../../assets/img/Project1/3.png";
import project1img4 from "../../assets/img/Project1/4.png";
import project1img5 from "../../assets/img/Project1/5.png";
import project1img1mobile from "../../assets/img/Project1/phone1.png";
import project1img2mobile from "../../assets/img/Project1/phone2.png";
import project1img3mobile from "../../assets/img/Project1/phone3.png";
import project1img4mobile from "../../assets/img/Project1/phone4.png";
import project1img5mobile from "../../assets/img/Project1/phone5.png";
import project2img1 from "../../assets/img/Project2/1.png";
import project2img2 from "../../assets/img/Project2/2.png";
import project2img3 from "../../assets/img/Project2/3.png";
import project2img4 from "../../assets/img/Project2/4.png";

import project2img1mobile from "../../assets/img/Project2/mobile/1.png";
import project2img2mobile from "../../assets/img/Project2/mobile/2.png";

const Project = ({projectNumber, category, name, interval}) => {
    const projectData = {
        project1: [project1img1, project1img2, project1img3, project1img4, project1img5],
        project1phone: [project1img1mobile, project1img2mobile, project1img3mobile, project1img4mobile, project1img5mobile],
        project2:[project2img1,project2img2,project2img3,project2img4],
        project2mobile:[project2img1mobile,project2img2mobile]
    };
    return (
        <div className="project">
            <Carousel showStatus={false} autoPlay showThumbs={false} interval={interval}>
                {projectData[projectNumber].map((el) => {
                    return (
                        <div className="project__img" key={el}>
                            <img src={el} alt="Img Project" />
                        </div>
                    );
                })}
            </Carousel>
            {(category || name) && (
                <div className="project__text">
                    <h3>{category}</h3>
                    <p>{name}</p>
                </div>
            )}
        </div>
    );
};

export default Project;
