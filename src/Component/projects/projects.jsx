import SendButton from "../button/send-button";
import Project from "./project";

const Projects = () => {
    return (
        <div className="projects">
            <h2>My projects</h2>

            <SendButton text="See all projects " icon="Arrow2" onClick={()=> window.open("https://bitbucket.org/Hrynevych/")} />

            <div className="projects__item">
                <div className="projects__first-item">
                    <Project
                        projectNumber={"project1"}
                        category="Web App"
                        name="| Quiz"
                        interval={5000}
                    />
                    <div className="projects__send">
                        <SendButton text="See more" icon="Arrow2" onClick={()=> window.open("https://melodic-haupia-ac992c.netlify.app")} />
                    </div>
                </div>
                <div className="projects__second-item">
                    <Project
                        projectNumber={"project2"}
                        category="Portfilo"
                        name="| John Doe"
                        interval={3000}
                    />
                    <div className="projects__send">
                        <SendButton text="See more" icon="Arrow2" onClick={()=> window.open("https://ecstatic-kare-85373d.netlify.app")} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Projects;
